//
//  CollectionViewCell.m
//  Collection_View_Xibs_v2
//
//  Created by Bruno Tavares on 26/01/15.
//  Copyright (c) 2015 Bruno Tavares. All rights reserved.
//

#import "CollectionViewCell.h"

@interface CollectionViewCell ()

@property (nonatomic, strong) NSString *photoString;

@end

@implementation CollectionViewCell

- (id)initWithFrame:(CGRect)aRect
{
    self = [super initWithFrame:aRect];
    {
        self.imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        self.nameLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        
        self.nameLabel.backgroundColor = [UIColor whiteColor];
        self.nameLabel.textColor = [UIColor blackColor];
        
        self.imageView.translatesAutoresizingMaskIntoConstraints = NO;
        self.nameLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self addSubview:self.imageView];
        [self addSubview:self.nameLabel];
        
        NSDictionary *viewsDictionary = @{@"imageView":self.imageView.viewForBaselineLayout,
                                          @"nameLabel":self.nameLabel.viewForBaselineLayout};
        
        NSArray *constraint_V_imageView = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-2-[imageView]-2-|"
                                                                                  options:0
                                                                                  metrics:nil
                                                                                    views:viewsDictionary];
        
        NSArray *constraint_H_imageView = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-2-[imageView]-2-|"
                                                                                  options:0
                                                                                  metrics:nil
                                                                                    views:viewsDictionary];
        
        NSArray *constraint_V_nameLabel = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[nameLabel]-2-|"
                                                                                  options:0
                                                                                  metrics:nil
                                                                                    views:viewsDictionary];
        
        NSArray *constraint_H_nameLabel = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[nameLabel]-2-|"
                                                                                  options:0
                                                                                  metrics:nil
                                                                                    views:viewsDictionary];
        
        [self addConstraints:constraint_H_imageView];
        [self addConstraints:constraint_V_imageView];
        [self addConstraints:constraint_H_nameLabel];
        [self addConstraints:constraint_V_nameLabel];
    }
    return self;
}

@end
