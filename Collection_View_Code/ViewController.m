//
//  SecondViewController.m
//  Nav_Bar_Table_View_ObjC_v2
//
//  Created by Bruno Tavares on 20/01/15.
//  Copyright (c) 2015 Bruno Tavares. All rights reserved.
//

#import "ViewController.h"

NSUInteger const kLabelHeight = 300;
NSUInteger const kLabelWidth = 70;

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor blackColor];
    self.view.backgroundColor = [self.view.backgroundColor colorWithAlphaComponent:0.8f];
    
    self.exitButton = [[UIButton alloc] initWithFrame:CGRectZero];
    self.label = [[UILabel alloc] initWithFrame:CGRectZero];
    
    [self.exitButton.titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
    [self.exitButton setTitle:@"X" forState:UIControlStateNormal];
    [self.view addSubview:self.exitButton];
    
    self.label.textColor = [UIColor whiteColor];
    self.label.font = [UIFont boldSystemFontOfSize:36];
    [self.view addSubview:self.label];
    
    NSDictionary *viewsDictionary = @{@"button":self.exitButton,
                                      @"label":self.label,
                                      @"superview":self.view};
    
    self.exitButton.translatesAutoresizingMaskIntoConstraints = NO;
    self.view.translatesAutoresizingMaskIntoConstraints = NO;
    self.label.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.exitButton addTarget:self action:@selector(pressExitButton:) forControlEvents:UIControlEventTouchDown];
    
    NSArray *constraint_V_button = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-100-[button]"
                                                                          options:0
                                                                          metrics:nil
                                                                            views:viewsDictionary];
    
    NSArray *constraint_H_button = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[button]|"
                                                                          options:NSLayoutFormatAlignAllCenterY
                                                                          metrics:nil
                                                                            views:viewsDictionary];
    
    NSArray *constraint_V_label = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[superview]-(<=1)-[label]"
                                                                          options:NSLayoutFormatAlignAllCenterX
                                                                          metrics:nil
                                                                            views:viewsDictionary];
    
    NSArray *constraint_H_label = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[superview]-(<=1)-[label]"
                                                                          options:NSLayoutFormatAlignAllCenterY
                                                                          metrics:nil
                                                                            views:viewsDictionary];
    
    [self.view addConstraints:constraint_H_button];
    [self.view addConstraints:constraint_V_button];
    [self.view addConstraints:constraint_H_label];
    [self.view addConstraints:constraint_V_label];
}

- (IBAction)pressExitButton:(id)sender {
    
    self.view.hidden = true;
}
@end
