//
//  CollectionViewController.m
//  Collection_View_Xibs
//
//  Created by Bruno Tavares on 26/01/15.
//  Copyright (c) 2015 Bruno Tavares. All rights reserved.
//

#import "CollectionViewController.h"
#import "CollectionViewCell.h"
#import "ViewController.h"

@interface CollectionViewController ()

@property (strong, nonatomic) NSArray *collectionNames;
@property (strong, nonatomic) NSArray *collectionImages;
@property (strong, nonatomic) ViewController *viewController;

@end

@implementation CollectionViewController

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setItemSize:CGSizeMake(100, 100)];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    
    [self.collectionView setCollectionViewLayout:flowLayout];
    
    self.collectionView.backgroundColor = [UIColor whiteColor];
    
    // Instantiate data arrays
    self.collectionNames = @[@"Benfica", @"Porto", @"Sporting", @"Paços de Ferreira", @"Boavista", @"Belenenses", @"Benfica", @"Porto", @"Sporting", @"Paços de Ferreira", @"Boavista", @"Belenenses", @"Benfica", @"Porto", @"Sporting", @"Paços de Ferreira", @"Boavista", @"Belenenses", @"Benfica", @"Porto", @"Sporting", @"Paços de Ferreira", @"Boavista", @"Belenenses", @"Benfica", @"Porto", @"Sporting", @"Paços de Ferreira", @"Boavista", @"Belenenses"];
    self.collectionImages = @[@"4337681_vozr8.png", @"4337686_G6U2H.png", @"4454028_nDJPZ.png", @"Pacos-de-Ferreira-128x128.png", @"Boavista-icon.png", @"Logo_Belenenses.png", @"4337681_vozr8.png", @"4337686_G6U2H.png", @"4454028_nDJPZ.png", @"Pacos-de-Ferreira-128x128.png", @"Boavista-icon.png", @"Logo_Belenenses.png", @"4337681_vozr8.png", @"4337686_G6U2H.png", @"4454028_nDJPZ.png", @"Pacos-de-Ferreira-128x128.png", @"Boavista-icon.png", @"Logo_Belenenses.png", @"4337681_vozr8.png", @"4337686_G6U2H.png", @"4454028_nDJPZ.png", @"Pacos-de-Ferreira-128x128.png", @"Boavista-icon.png", @"Logo_Belenenses.png", @"4337681_vozr8.png", @"4337686_G6U2H.png", @"4454028_nDJPZ.png", @"Pacos-de-Ferreira-128x128.png", @"Boavista-icon.png", @"Logo_Belenenses.png"];
    
    [self.view addSubview:self.collectionView];
    
    NSDictionary *viewsDictionary = @{@"cView":self.collectionView};
    
    self.collectionView.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSArray *constraint_V_cView = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[cView]|"
                                                                          options:0
                                                                          metrics:nil
                                                                            views:viewsDictionary];
    
    NSArray *constraint_H_cView = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[cView]|"
                                                                          options:0
                                                                          metrics:nil
                                                                            views:viewsDictionary];
    
    [self.view addConstraints:constraint_H_cView];
    [self.view addConstraints:constraint_V_cView];
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.collectionNames.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    [[self collectionView] registerClass:[CollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    cell.nameLabel.text = self.collectionNames[indexPath.row];
    
    cell.imageView.image = [UIImage imageNamed:self.collectionImages[indexPath.row]];
    
    return cell;
}

#pragma mark <UICollectionViewDelegate>

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if(self.viewController == nil) {
        self.viewController = [[ViewController alloc] init];
        
        [self addChildViewController:self.viewController];
        [self.view addSubview:self.viewController.view];
        
        self.viewController.label.text = self.collectionNames[indexPath.row];
        
        self.viewController.view.translatesAutoresizingMaskIntoConstraints = NO;
        
        NSDictionary *viewsDictionary = @{@"view":self.viewController.view};
        
        NSArray *constraint_V_view = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[view]|"
                                                                             options:0
                                                                             metrics:nil
                                                                               views:viewsDictionary];
        
        NSArray *constraint_H_view = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|"
                                                                             options:0
                                                                             metrics:nil
                                                                               views:viewsDictionary];
        
        [self.view addConstraints:constraint_H_view];
        [self.view addConstraints:constraint_V_view];
    }
    
    self.viewController.view.hidden = false;
    self.viewController.label.text = self.collectionNames[indexPath.row];
}

@end
